#Functions
#Dillon
#3/27/2014

#print "fileSys[forCount] -",str(fileSys[forCount])+"\n i[1] -",str(i[1])+"\n forCount -",str(forCount)+"\n innerCount -",str(innerCount)+"\n i[0] -",str(i[0])

from getpass import getpass as pw
import os

def createAccount ():
    fileCheck = [line.strip() for line in open("users.txt","r")]

    while True:
        ifBroke = False
        username = raw_input("Please create a username... ")
        for i in fileCheck:
            if username == i[3:]:
                print "That username is taken, please choose another"
                ifBroke = True
                break
        if not ifBroke:
            break

    password = 0
    passwordV = 1

    while password != passwordV:
       password = pw("Please create a password... ")
       passwordV = pw("Please verify your password... ")

    username = "u: "+username
    password = "p: "+password

    userWrite = open("users.txt","a")
    userWrite.write(username+"\n")
    userWrite.write(password+"\n")
    print "Account successfully created..."

    return username[3:]

def createFileSyst(ui):
    os.mkdir(ui)
    useString = "cp infra.txt "+ui
    os.system(useString)
    os.chdir(ui)

def takeCheck (ui):
	fileCheck = [line.strip() for line in open("users.txt","r")]
	userList = []
	passwordList = []
	for i in fileCheck:
		if i.startswith("u:"):
			userList.append(i[3:])
		elif i.startswith("p:"):
			passwordList.append(i[3:])
	for c in enumerate(userList):
		if ui[0] == c[1]:
			for b in enumerate(passwordList):
				if ui[1] == b[1] and c[0] == b[0]:
					return True

def getFormattedAll ():
    fileSys = str([line for line in open("infra.txt","r")])
    fileSys = fileSys[2:]
    fileSys = fileSys[:(len(fileSys)-4)]
    return fileSys

def formatIntoList(fc):
    fc = list(fc)
    count = 0
    return fc
#finish this

def getCDName (count):
    fileSys = list(getFormattedAll())
    while count != len(fileSys)-1:
        if fileSys[count] == "[":
            innerCount = count
            forCount = count
            while True:
                if fileSys[innerCount] == "|":
                    currentFolderName = ""
                    for i in enumerate(fileSys):
                        if fileSys[forCount] == i[1] and forCount <= i[0] and innerCount >= i[0]:
                            currentFolderName += fileSys[forCount]
                        if not forCount == len(fileSys)-1:
                            forCount += 1
                    currentFolderName = currentFolderName[(len(currentFolderName)+3)-len(currentFolderName):]
                    currentFolderName = currentFolderName[:len(currentFolderName)-1]
                    print "cwd: "+currentFolderName
                    return count
                innerCount += 1
        count += 1

def getDContents (dCount):
    fileSys = getFormattedAll()
    while True:
        if fileSys[dCount] == "[":
            innerCount = dCount
            forCount = dCount
            theCount = dCount
            while True:
                if fileSys[innerCount]+fileSys[innerCount+1]+fileSys[innerCount+2] == "/0]":
                    currentFolderContents = ""
                    for i in enumerate(fileSys):
                        if fileSys[forCount] == i[1] and forCount <= i[0] and innerCount >= i[0]:
                            currentFolderContents += fileSys[forCount]
                        if not forCount == len(fileSys)-1:
                            forCount += 1
                    while currentFolderContents[theCount] != "|":
                        theCount += 1
                    currentFolderContents = currentFolderContents[:len(currentFolderContents)-1]
                    currentFolderContents = currentFolderContents[theCount+1:]
                    print formatIntoList(currentFolderContents)
                    return
                innerCount += 1
        dCount += 1
#add number detection for the filesystem

def all ():
    count = 0
    while True:
        inp = raw_input("-> ")
        if inp:
            try:
                if inp == "logout":
                    return
                if inp[:3] == "f-d":
                    count = getCDName(count)
                    getDContents(count)
                    count += 1
                else:
                    print "Command currently unrecognized, try again on a newer version"
            except:
                IndexError


