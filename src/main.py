#Main Code
#Dillon
#4/10/2014

from functions import *

innerLoopFlag = False
while True:
    if innerLoopFlag:
        break
    makeNew = raw_input("Login || Create New Account... ")
    if makeNew == "c-na":
        ui = createAccount()
        createFileSyst(ui)
        break
    if makeNew == "l-a":
        ui = [raw_input("Username... ").strip(),pw("Password... ").strip()]
        if not os.path.isdir(ui[0]):
            print "It appears that this username doesn't exist, please log in legitimately or create an account"
            continue
        if ui:
            while True:
                if takeCheck(ui):
                    print "Successful login..."
                    innerLoopFlag = True
                    break
                else:
                    print "Failure logging in, retrying..."

all()
print "Goodbye", ui[0]
